# DKIM Milter

The DKIM Milter project has moved to the [Codeberg] platform:

<https://codeberg.org/glts/dkim-milter>

Please update your links.

[Codeberg]: https://codeberg.org
